/* Garden watering system
 * Author: Istvan Gaal
 * Device: Arduino Nano
 */
#define LIGHT_LIMIT_VALUE 200
#define MOISTURE_LIMIT_VALUE 500
#define BATTERY_CORRECTION_VALUE 68.2

//Labkiosztas / PINS
int sensorGround = A0;
int sensorLight = A1;
float sensorBatteryVoltage = A2;
int waterlevelMAX = 4;
int waterlevelMID = 3;
int waterlevelMIN = 2;
int ledPin = 13;
int waterpumpRelay = 12;

//Variables
int senValGround = 0;
int senValLight = 0;
float senValBatteryVoltage = 0.0;
bool waterlevelMaxVal = false;
bool waterlevelMidVal = false;
bool waterlevelMinVal = false;

void setup() {
  // declare the pins 
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);
  pinMode(waterpumpRelay, OUTPUT);
  pinMode(waterlevelMAX, INPUT_PULLUP);
  pinMode(waterlevelMID, INPUT_PULLUP);
  pinMode(waterlevelMIN, INPUT_PULLUP);
  
}

void loop() {
  // read the values from the sensors:
  senValGround = analogRead(sensorGround);
  senValLight = analogRead(sensorLight);
  senValBatteryVoltage = analogRead(sensorBatteryVoltage);
  waterlevelMaxVal = !digitalRead(waterlevelMAX);
  waterlevelMidVal = !digitalRead(waterlevelMID);
  waterlevelMinVal = !digitalRead(waterlevelMIN);

  //Print datas for me (only information)
  Serial.println();
  Serial.println();
  Serial.print("Ground_sensor_value:");
  Serial.println(senValGround);
  Serial.print("Light_sensor_value:");
  Serial.println(senValLight);
  Serial.print("Battery_voltage:");
  Serial.print(senValBatteryVoltage/BATTERY_CORRECTION_VALUE);
  Serial.println("V");
  Serial.print("WaterlevelMAX_sensor:");
  Serial.println(waterlevelMaxVal);
  Serial.print("WaterlevelMID_sensor:");
  Serial.println(waterlevelMidVal);
  Serial.print("WaterlevelMIN_sensor:");
  Serial.println(waterlevelMinVal);
  delay(6000);

  //Waterpump control
  if (waterlevelMinVal == 1 && senValGround >= MOISTURE_LIMIT_VALUE && senValLight <= LIGHT_LIMIT_VALUE ){
    digitalWrite(waterpumpRelay, HIGH);
    delay(105000);
    digitalWrite(waterpumpRelay, LOW);
    }
   else{
     digitalWrite(waterpumpRelay, LOW);
  }
 
  delay(7200000);
}
