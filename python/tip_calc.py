print("Welcome to the tip calculator")

bill = input("What was the total bill? $")

bill_as_float = float(bill)

tip = input("What percentage tip would you like to give? 10, 12, or 15?")

tip_as_float = float(tip) / 100

people = input("How many people to split the bill?")

people_as_int = int(people)

amount = (bill_as_float + (bill_as_float * tip_as_float)) / people_as_int
amount_rounded = round(amount, 2)
print(f"Each person should pay: ${amount_rounded}")