import random

user_choice = input("WHat is Your choice? Heads or Tails?\n")

lower_user_choice = user_choice.lower()

user_choice_num = 0

if lower_user_choice == "heads":
    user_choice_num = 1
elif lower_user_choice == "tails":
    user_choice_num = 0
else:
    print("Your choice is different!\n")
    
machine_choice = random.randint(0, 1)

if machine_choice == 1:
    print("Heads!\n")
else:
    print("Tails!\n")

if machine_choice == user_choice_num:
    print("You win!\n")
else:
    print("You loose!\n")