student_scores = input("Input a list of student scores \n").split()
for n in range( 0, len(student_scores)):
    student_scores[n] = int(student_scores[n])
print(student_scores)

maximum_score = 0

for n in range( 0, len(student_scores)):
    if int(student_scores[n]) > maximum_score:
        maximum_score = int(student_scores[n]) 

print(f"The highest score in the class is: {maximum_score}")