print("Welcome to the Love Calculator!")

name1 = input("What is your name? \n")
name2 = input("What is their name? \n")

name1 = name1.lower() 
name2 = name2.lower()
name =name1+name2

t = name.count("t")
r = name.count("r")
u = name.count("u")
e = name.count("e")
l = name.count("l")
o = name.count("o")
v = name.count("v")

first_digit = str(t + r + u + e)
second_digit = str(l + o + v + e)

number = int(first_digit+second_digit)

if number < 10 or number > 90:
    print(f"Your score is {number}, you go together like coke and mentos.\n")
elif number >= 40 and number <= 50:
    print(f"Your score is {number}, you are alright together.\n")
else:
    print(f"Your score is {number}.")