age = input("what is your current age?\n")

your_days = int(age) * 365
your_weeks = int(age) * 52
your_months = int(age) * 12

ninety_years_old_days = 90 * 365
ninety_years_old_weeks = 90 * 52
ninety_years_old_months = 90 * 12

days = ninety_years_old_days - your_days
weeks = ninety_years_old_weeks - your_weeks
months = ninety_years_old_months - your_months

print(f"You have {days} days, {weeks} weeks and {months} months left.")