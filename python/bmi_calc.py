weight = input("How many kilos are you?\n")
height = input("How many tall are you in meter?\n")

bmi = float(weight) / (float(height) ** 2)

if bmi < 18.5 :
    print("you are underweight!\n")
elif bmi > 18.5 and bmi < 25:
    print("you are normal weight!\n")
elif bmi > 25 and bmi < 30:
    print("You are overweight!\n")
elif bmi > 30 and bmi < 35:
    print("You are obese!\n")
elif bmi > 35:
    print("You are clinically obese!\n")
else:
    print(".........")

print("Your BMI is:" + str(int(bmi)))