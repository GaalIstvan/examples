year = int(input("which year do you want to check?\n"))
           
if (year % 4 == 0):
    if (year % 100 == 0):
        if (year % 400 == 0):
            print("This year is a leap year!\n")
        else:
            print("This year is not a leap year!\n")
    else:
        print("This year is a leap year!\n")
else:
        print("This year is not a leap year!\n")
