import random

# Rock
rock = """
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
"""

# Paper
paper = """
     _______
---'    ____)____
           ______)
          _______)
         _______)
---.__________)
"""

# Scissors
scissors = """
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
"""

user_choice_int = int(input("What do you choose? Type 0 for rock, 1 for paper or 2 for scissors.\n"))
types = [rock, paper, scissors]
print("Your choice: \n")
print(types[user_choice_int])

machine_choice = random.randint(0, 2)
print("machine choice: \n")
print(types[machine_choice])


if user_choice_int == 0:
    if machine_choice == 0:
        print("Draw!")
    elif machine_choice == 2:
        print("You win!")
    elif machine_choice == 1:
        print("You lose!")
    else:
        print(".....")

if user_choice_int == 1:
    if machine_choice == 1:
        print("Draw!")
    elif machine_choice == 0:
        print("You win!")
    elif machine_choice == 2:
        print("You lose!")
    else:
        print(".....")

if user_choice_int == 2:
    if machine_choice == 2:
        print("Draw!")
    elif machine_choice == 1:
        print("You win!")
    elif machine_choice == 0:
        print("You lose!")
    else:
        print(".....")
